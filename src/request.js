import axios from 'axios'

const proxy = CONFIG.proxy
const baseUrl = CONFIG.baseUrl

export default {
    getMapData() {
        return axios.get(`${baseUrl}MapNum`)
    },
    getStatisticData(params) {
        return axios.get(`${baseUrl}JsonServer`, { params })
    },
    getChartsData(params) {
        return axios.get(`${baseUrl}RightViewServer`, { params })
    },
    getCompanies(params) {
        return axios.get(`${baseUrl}CompanyInformation`, { params })
    },
    getProjects(params) {
        return axios.get(`${baseUrl}ObjectInformation`, { params })
    },
    getPersons(params) {
        return axios.get(`${baseUrl}PeopleInformation`, { params })
    },
    getTableDataByKeyword(params) {
        return axios.get(`${baseUrl}SearchFunction`, { params })
    },
    search(params) {
        return axios.get(`${baseUrl}MapSearch`, { params })
    },
    getDetail(params) {
        return axios.get(`${baseUrl}Information`, { params })
    },
    // PIO 检索
    searchPOI(params) {
        return axios.get('http://api.map.baidu.com/place/v2/search', { params })
    },
    // 地理编码
    geocoding(params) {
        return axios.get('/geocoding', { params })
    },
    // 地理编码，走代理
    geocodingByProxy(params) {
        let url = `http://api.map.baidu.com/geocoding/v3/?address=${params.address}&ak=${params.ak}&output=json`
        return axios.get(proxy + url)
    }
}
