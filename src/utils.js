export default {
    // 防抖
    debounce(func, wait) {
        let timer = null
        return function() {
            clearTimeout(timer)
            timer = setTimeout(() => {
                func.applay(this, arguments)
            }, wait)
        }
    },
    // 节流
    throttle(func, wait) {
        let lastTime = 0
        return function() {
            let current = +(new Date())
            if (current - lastTime > wait) {
                func.applay(this, arguments)
                lastTime = current
            }
        }
    }
}
