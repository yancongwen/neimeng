import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ECharts from 'vue-echarts'
import {
  Message,
  Input,
  Select,
  Option,
  Page,
  Table,
  Icon,
  Tooltip,
  LoadingBar,
  Carousel,
  CarouselItem
} from 'iview'
import 'iview/dist/styles/iview.css'
import 'echarts/lib/chart/bar'
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/title'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/dataset'

Vue.component('Input', Input)
Vue.component('Select', Select)
Vue.component('Option', Option)
Vue.component('Page', Page)
Vue.component('Table', Table)
Vue.component('Icon', Icon)
Vue.component('Tooltip', Tooltip)
Vue.component('Carousel', Carousel)
Vue.component('CarouselItem', CarouselItem)
Vue.component('v-chart', ECharts)
Vue.prototype.$Message = Message
Vue.prototype.$Loading = LoadingBar

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')