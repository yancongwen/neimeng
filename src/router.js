import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Table from './views/Table.vue'
import MapCom from './views/Map.vue'
import Detail from './views/Detail.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        index: 0
      }
    },
    {
      path: '/table/:type/:keyword/:index',
      name: 'table',
      component: Table,
      props: true,
      meta: {
        index: 1
      }
    },
    {
      path: '/detail/:type/:name',
      name: 'detail',
      component: Detail,
      props: true,
      meta: {
        index: 2
      }
    },
    {
      path: '/map',
      name: 'map',
      component: MapCom,
      meta: {
        index: 1
      }
    }
  ]
})
